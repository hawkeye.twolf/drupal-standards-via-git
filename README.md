# Automatically install a git pre-commit hook to enforce Drupal coding standards

Look, you probably shouldn't do this. You really ought to use GrumPHP via
[vijaycs85/drupal-quality-checker](https://packagist.org/packages/vijaycs85/drupal-quality-checker)
(see [How to Enforce Drupal Coding Standards via Git](https://www.lullabot.com/articles/how-enforce-drupal-coding-standards-git)).  But if that doesn't work for [whatever
reason](https://github.com/phpro/grumphp/issues/592), these steps are here to
help.

1. Download the Drupal coding standards file:
    ```
    composer require --dev drupal/coder
    ```

1. Automatically register the drupal coding standards with php-code-sniffer:
    ```
    composer require --dev dealerdirect/phpcodesniffer-composer-installer
    ```

1. Copy this repo's two scripts—`add_pre_commit_hook.sh` and `lint.sh`—to your
   project. The rest of this documentation assumes you've placed them in a
   `scripts` folder at the same level as your `composer.json`. If that's not
   the case, adjust the scripts and the following steps accordingly.

1. Edit `add_pre_commit_hook.sh` to match your project's directory structure:
    - If `lint.sh` doesn't live in a `scripts` folder at the same level as your
      composer.json, adjust the path in `LINT_COMMAND` to point to the correct
      location relative to `composer.json`.
    - Replace all instances of `path/to/repo/root` with the path to the root of
      your git repository relative to `composer.json`. For example, if your
      composer.json is in a sub-folder one level down, you would use '..'.

1. Edit `lint.sh` to match your project's directory structure:
    - Replace `path/to/composer-json` with the path of your `composer.json`,
      relative to the root of your git repository.
    - If your `scripts` folder doesn't live at the same level as
      `composer.json`, adjust line 7 to point to the correct location relative
      to `composer.json`.
    - Replace all instances of `path/to/drupal` with the path to the folder
      containing `index.php`, relative to `composer.json`.

1. Manually edit your `composer.json` to execute `add_pre_commit_hook.sh` on
   `composer install` and `composer update`. See this repo's
   `composer.json.patch` for an example.

1. Run `composer install`, and marvel!

#!/bin/sh

# Initialize the pre-commit script if there isn't one yet.
if [ ! -f path/to/repo/root/.git/hooks/pre-commit ]; then
  echo '#!/bin/sh\n' > path/to/repo/root/.git/hooks/pre-commit
  chmod u+x path/to/repo/root/.git/hooks/pre-commit
fi

# Append our linting command to the pre-commit hook.
LINT_COMMAND="sh scripts/lint.sh"
if ! egrep --silent "^${LINT_COMMAND}$" path/to/repo/root/.git/hooks/pre-commit; then
  echo "${LINT_COMMAND}" >> path/to/repo/root/.git/hooks/pre-commit
fi

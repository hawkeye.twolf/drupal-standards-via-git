#!/bin/sh

cd path/to/composer-json
vendor/bin/phpcs                \
  --standard=Drupal             \
  --ignore=*.css                \
  scripts                       \
  path/to/drupal/sites          \
  path/to/drupal/modules/custom \
  path/to/drupal/themes
RESULT=$?
if [ $RESULT != 0 ]; then
  printf '\033[1;33mTo skip commit checks, add -n or --no-verify flag to commit command\n\n'
fi
exit $RESULT
